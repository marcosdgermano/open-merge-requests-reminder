const _MS_PER_DAY = 1000 * 60 * 60 * 24;

const getOpenDays = (mrCreatedAt) => {
  const mrDate = new Date(mrCreatedAt);
  const dateNow = new Date();

  const utc1 = Date.UTC(mrDate.getFullYear(), mrDate.getMonth(), mrDate.getDate());
  const utc2 = Date.UTC(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

module.exports = { getOpenDays };