var axios = require('axios');
var getMergeRequests = require('./controllers/merge-requests');
const BOT_API = 'https://api.telegram.org/bot1672686699:AAG_4t0Rie4fjPz2JMxA59i0YMdoTqoNKgM/sendMessage';

const formatMergeRequestMessage = mr => {
  return `**Título**: _${mr.title}_ \n*Autor*: ${mr.author} \n[Link para o PR](${mr.url}) \n*Aberto há ${mr.openDays} dias*`;
}

const main = async () => {
  try {
    const mergeRequests = await getMergeRequests();
    const title = '*As PRs abaixo estão abertas a mais de um dia, vamos dar uma olhada!*\n============================================'
    const message = `${title}\n\n\n${mergeRequests.map(mr => `${formatMergeRequestMessage(mr)} \n\n`)}`
    
    await axios.post(BOT_API, {
        chat_id: '809266684',
        text: message,
        parse_mode: 'markdown'
      });
  } catch(e) {
    throw new Error(e);
  }
}

main();
