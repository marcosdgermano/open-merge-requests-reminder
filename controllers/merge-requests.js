const axios = require('axios');
const { get } = require('lodash');
var { getOpenDays } = require('../helpers/date');

const GITLAB_API = 'http://gitlab.internal.b2w.io/api/v4/projects/3158/merge_requests?private_token=dWJkt29o3BSjnmSZnyko&state=opened';

module.exports = async () => {
  try {
    const { data: mergeRequests } = await axios.get(GITLAB_API);
    
    return mergeRequests.map(mr => {
      return {
        title: mr.title,
        author: mr.assignee.name,
        openDays: getOpenDays(mr.created_at),
        labels: mr.labels,
        url: mr.web_url,
      }
    }).sort((a, b) => b.openDays - a.openDays)
      .filter(mr => mr.openDays > 1 && get(mr, ['labels'], []).includes('team: solving'));
  } catch(e) {
    throw new Error(e);
  }
}
